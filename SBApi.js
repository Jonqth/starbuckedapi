/*
// Requiring the world
*/
var express = require('express'),
    starbucks = require('./routes/starbucks'),
    sb = express();

/*
// Configuring Basics
*/
sb.configure(function () {
    sb.use(express.logger('jonqt'));
    sb.use(express.bodyParser());
});

/*
// Earinating (listening)
*/
sb.get('/starbucks', starbucks.findAll);
sb.get('/starbucks/:position', starbucks.findByNearest);
 
// Hello ThreeThousand 
sb.listen(3000);
console.log('Listening on port 3000...');