/*
// Requiring mongoStupid
*/
var mongo = require('mongodb');
 
/*
// Configuring it
*/
var Server = mongo.Server,
    DB = mongo.Db,
    BSON = mongo.BSONPure;
 

/*
// Declaring DB
*/
var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new DB('starbucksdb', server);

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'StarbuckedDB' database");
        populateDB();
        db.collection('starbucks', {safe:true}, function(err, collection) {
            if (err) {
                console.log("The 'starbucks' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }
        });
    }
});

var populateDB = function() {

    var starbucks = [
    {
        name: "Place Blanche",
        adress: "5 place Blanche",
        latitude: "48.883741",
        longitude: "2.332277",
        city: "Paris",
        postal: "75018"
    }];
 
    db.collection('starbucks', function(err, collection) {
        collection.insert(starbucks, {safe:true}, function(err, result) {});
    });
 
};
