/*
// Requiring mongoStupid
*/
var starbucks = require('./database/database');

exports.findAll = function(req, res) {
    db.collection('starbucks', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};


exports.findByNearest = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving starbucks: ' + id);
    db.collection('starbucks', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {
            res.send(item);
        });
    });
};